<?php

if (!function_exists('since'))
{
	function since($date)
	{
		return \Carbon\Carbon::parse($date)->diffForHumans();
	}
}