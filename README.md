## About Laravel

Task management frontend is part of task management project to have function to show or display task list in front end

## How to Install

- Git clone : https://gitlab.com/agungsetiawan11/task-management-app.git
- Go to directory
-- composer install
-- npm install
- npm run dev
- Import SQL file in folder doc into MySQL
- cp .env.example
- php artisan key:generate
- change DB credential in .env file following MySQL enviroment.
- php artisan migrate.
- php artisan db:seed --class=UserSeeder.
- php artisan serve
- open Browser and pointing to URL http://localhost:8000/
- there's 2 type existing user : admin and user.
- Admin login : admin@email.com password : admin_123
- User login : user_1@email.com password : user_123

## Alternative for dump SQL

Otherwise using migration and seeder, you can dump SQL file inside doc folder into MySQL.

## Creator

Agung Setiawan

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
