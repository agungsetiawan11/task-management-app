<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\Facades\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();

        /* define a admin user role */
        $gate::define('isAdmin', function($user) {
           return $user->role == 'admin';
        });
       
        /* define a user role */
        $gate::define('isUser', function($user) {
            return $user->role == 'user';
        });

        /*
        | Task
        */
        $gate::define('create-Task', function ($user) {
            return in_array($user->role, ['admin', 'user']);
        });

        $gate::define('edit-Task', function ($user) {
            return in_array($user->role, ['admin', 'user']);
        });

        $gate::define('remove-Task', function ($user) {
            return $user->role === 'admin';
        });

        /*
        | User
        */
        $gate::define('create-User', function ($user) {
            return $user->role === 'admin';
        });

        $gate::define('edit-User', function ($user) {
            return in_array($user->role, ['admin', 'user']);
        });

        $gate::define('remove-User', function ($user) {
            return $user->role === 'admin';
        });
    }
}
