<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Task extends Model
{
    protected $table = 'tasks';

    use HasFactory;

    public function assignee_user()
    {
        return $this->belongsTo(User::class, 'assignee');
    }
}
