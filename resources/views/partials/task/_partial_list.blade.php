<table class="tbls table">
    <thead>
        <tr>
            <th width="2%">No.</th>
            <th width="20%">Title</th>
            <th width="20%">Description</th>
            <th>Start</th>
            <th>End</th>
            <th>Status</th>
            <th>Assignee</th>
            @if ($swc == 1)
                <th>_Action_</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @php $cnt = 1 @endphp
        @foreach($dataList as $val)
            <tr>
                <td>{{ $cnt }}</td>
                <td>{{ $val['title'] }}</td>
                <td>{{ $val['description'] }}</td>
                <td>
                    <div>{{ date('d-m-Y', strtotime($val['start_date'])) }}</div>
                    <div>{{ date('H:i', strtotime($val['start_time'])) }}</div>
                </td>
                <td>
                    <div>{{ (!empty($val['end_date'])) ? date('d-m-Y', strtotime($val['end_date'])) : '-' }}</div>
                    <div>{{ (!empty($val['end_time'])) ? date('H:i', strtotime($val['end_time'])) : '-' }}</div>
                </td>
                <td>{{ $val['status'] }}</td>
                <td>{{ $val['assignee_user']['name'] }}</td>
                @if ($swc == 1)
                    <td>
                        <div class="dropdown text-center">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-cogs"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ url('task/edit/' . $val['id']) }}"><i class="fa fa-edit"></i> Edit</a>
                                <a class="dropdown-item" href="javascript:void()" onclick="removeTask({{ $val['id'] }})"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                    </td>
                @endif
            </tr>
            @php $cnt++ @endphp
        @endforeach
    </tbody>
</table>