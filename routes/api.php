<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->get('/user', [App\Http\Controllers\UserController::class, 'AuthRouteAPI']);

Route::group(['prefix' => 'v1'], function()
{
    Route::get('task-list', [App\Http\Controllers\APIController::class, 'getTaskList']);

    Route::get('task-detail', [App\Http\Controllers\APIController::class, 'getTaskDetail']);
});