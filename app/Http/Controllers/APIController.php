<?php

namespace App\Http\Controllers;

use Auth;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Task;

class APIController extends Controller
{
    public function getTaskList(Request $request)
    {
        $task = Task::with('assignee_user')
                    ->orderBy('created_at', 'DESC')
                    ->get()
                    ->toArray();

        $no = 1;
        foreach($task as $val)
        {
            $result[] = [
                $no,
                '<a href="/task/detail/' . $val['id'] . '">' . $val['title'] . '</a>',
                $val['description'],
                date('d-m-Y', strtotime($val['start_date'])) . ' ' . date('H:i', strtotime($val['start_time'])),
                date('d-m-Y', strtotime($val['end_date'])) . ' ' . date('H:i', strtotime($val['start_time'])),
                $val['status'],
                $val['assignee_user']['name']
            ];
            $no++;
        }

        return Response::json([
            'data'   => $result
        ], 200)
        ->withHeaders([
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getTaskDetail(Request $request)
    {
        if ($request->input('id'))
        {
            $task = Task::with('assignee_user')
                        ->find($request->input('id'));

            if ($task)
            {
                return Response::json([
                    'success'    => true,
                    'msg'       => null,
                    'data'      => $task->toArray()
                ], 200)
                ->withHeaders([
                    'Content-Type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
                ]);
            } else {
                return Response::json([
                    'success'    => false,
                    'msg'       => 'Record not found'
                ], 404)
                ->withHeaders([
                    'Content-Type' => 'application/json',
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
                ]);
            }
        } else {
            return Response::json([
                'success'    => false,
                'msg'       => 'Missing ID'
            ], 404)
            ->withHeaders([
                'Content-Type' => 'application/json',
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
        }
    }
}
