@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Task -->
    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Task</div>
                <div class="card-body">
                    <div>@include('partials/user/_partial_user_list', ['dataList' => $user, 'swc' => 1])</div>
                    <a href="{{ url('user/create') }}" class="btn btn-md btn-primary"><i class="fa fa-plus"></i> Create New User</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script type="text/javascript">

    function removeUser(ids)
    {
        var r = confirm('Are you sure remove this user ?');

        if (r == true)
        {
            $.post( "{{ url('/user/remove') }}", {
                "_token": "{{ csrf_token() }}",
                'ids': ids
            }, function( data ) {
                if (data.succes)
                {
                    window.location.reload(true);
                }
            });
        }
    }

</script>
