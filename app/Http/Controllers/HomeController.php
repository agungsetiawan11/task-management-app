<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        if (Gate::allows('isAdmin')) 
        {
            $task = Task::with('assignee_user')
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();

            $user = User::orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();
        } else {
            $task = Task::with('assignee_user')
                        ->where('assignee', $user->id)
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();
        }

        return view('home')
                ->with('task', $task)
                ->with('user', $user);
    }
}
