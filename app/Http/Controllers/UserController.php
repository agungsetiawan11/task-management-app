<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Gate::allows('isAdmin')) 
        {
            $user = User::orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();

            return view('page.user.index')
                    ->with('user', $user);
        } else {
            abort(404);
        }
    }

    public function create()
    {
        if (Gate::allows('isAdmin')) 
        {
            return view('page.user.add');
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        if($request->method() == 'POST' && Gate::allows('isAdmin'))
        {
            $rules = [
                'user_email'     => 'required|email|unique:users,email',
                'user_name'      => 'required',
                'role'           => 'required',
                'user_password'  => 'required',
            ];

            $messages = [];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails())
            {
                return back()->with('error', $validator->errors());
            } else {
                $data = new User;

                $data->email             = $request->input('user_email');
                $data->name              = $request->input('user_name');
                $data->email_verified_at = date('Y-m-d H:i:s');
                $data->password          = Hash::make($request->input('user_password'));
                $data->role              = $request->input('role');

                $data->save();

                return redirect('user/index')->with('success','Data is saved successfully!');
            }
        } else {
            abort(404);
        }
    }

    public function edit($ids = 0 )
    {
        if ($ids > 0)
        {
            $q = User::find($ids);

            if ($q)
            {
                return view('page.user.edit')
                        ->with('data', $q);
            } else {
                return redirect()
                        ->route('user/create')
                        ->with('warning', 'Sorry user not found');
            }
        } else {
            return redirect()
                    ->route('user/create')
                    ->with('warning', 'Error request');
        }
    }

    public function update(Request $request)
    {
        if($request->method() == 'POST')
        {
            $rules = [
                'user_email'     => 'required',
                'user_name'      => 'required',
                'role'           => 'required',
            ];

            $messages = [];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails())
            {
                return back()->with('error', $validator->errors());
            } else {
                $data = User::find($request->input('ids'));

                $data->email             = $request->input('user_email');
                $data->name              = $request->input('user_name');
                $data->email_verified_at = date('Y-m-d H:i:s');

                if (!empty($request->input('user_password')))
                {
                    $data->password      = Hash::make($request->input('user_password'));
                }

                $data->role              = $request->input('role');

                $data->save();

                return back()->with('success','Data is saved successfully!');
            }
        }
    }

    public function remove(Request $request)
    {
        if (Gate::allows('isAdmin'))
        {
            $q = User::find($request->input('ids'));

            if ($q)
            {
                $q->delete();

                return Response::json([
                    'succes' => TRUE,
                    'msg'    => NULL,
                    'date'   => date('d-m-Y H:i:s'),
                ], 200)
                ->withHeaders([
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
                ]);
            } else {
                return Response::json([
                    'succes' => FALSE,
                    'msg'    => 'Record not found',
                    'date'   => date('d-m-Y H:i:s'),
                ], 404)
                ->withHeaders([
                    'Access-Control-Allow-Origin' => '*',
                    'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
                ]);
            }
        } else {
            return Response::json([
                'succes' => FALSE,
                'msg'    => 'Page not found',
                'date'   => date('d-m-Y H:i:s'),
            ], 404)
            ->withHeaders([
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
        }
    }
    
    public function AuthRouteAPI(Request $request){
        return $request->user();
    }
}